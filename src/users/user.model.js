const mongoose = require('mongoose');

const User = new mongoose.Schema({
  name: String,
  phoneNumber: String,
  verifyCode: Number,
  image: String,
});

const Model = mongoose.model('user', User);

module.exports = Model;
