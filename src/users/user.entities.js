const User = require('./user.model');

exports.findByPhoneNumber = (phoneNumber) => {
  return User.findOne({ phoneNumber });
};

exports.findById = (id) => {
  return User.findById(id);
};
