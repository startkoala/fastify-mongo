const joi = require('@hapi/joi');

const joiOptions = {
  abortEarly: false, // return all errors
  convert: true, // change data type of data to match type keyword
  allowUnknown: false, // remove additional properties
  noDefaults: false,
};

const joiBodySchema = joi.object().keys({
  name: joi
    .string()
    .required()
    .error((errors) => {
      for (const err of errors) {
        switch (err.code) {
          case 'string.empty':
            err.message = 'نام الزامیست';
            break;
          default:
            err.message = 'ورودی نا معتبر';
            break;
        }
      }
      return errors;
    }),
  phoneNumber: joi
    .string()
    .length(11)
    .trim()
    .required()
    .error((errors) => {
      for (const err of errors) {
        switch (err.code) {
          case 'string.empty':
            err.message = 'شماره موبایل الزامیست';
            break;
          default:
            err.message = 'شماره موبایل نا معتبر';
            break;
        }
      }
      return errors;
    }),
  image: joi.string().error((errors) => {
    for (const err of errors) {
      switch (err.code) {
        default:
          err.message = 'لطفا تصویر دیگری انتخاب کنید';
          break;
      }
    }
    return errors;
  }),
});

const schemaCompiler = (schema) => (data) => {
  const { error, value } = schema.validate(data, joiOptions);

  if (error) {
    return { error };
  }
  return value;
};

const schema = {
  body: joiBodySchema,
};

module.exports = {
  schema,
  schemaCompiler: schemaCompiler,
};
