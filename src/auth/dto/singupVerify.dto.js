const joi = require('@hapi/joi');

const joiOptions = {
  abortEarly: false, // return all errors
  convert: true, // change data type of data to match type keyword
  allowUnknown: false, // remove additional properties
  noDefaults: false,
};

const validateMobile = (value, helpers) => {
  if (value.substr(0, 2) !== '09') {
    return helpers.error('any.invalid');
  }

  return value;
};

const joiBodySchema = joi.object().keys({
  phoneNumber: joi
    .string()
    .length(11)
    .trim()
    .required()
    .custom(validateMobile, 'mobile')
    .error((errors) => {
      for (const err of errors) {
        switch (err.code) {
          case 'string.empty':
            err.message = 'شماره موبایل الزامیست';
            break;
          default:
            err.message = 'شماره موبایل نا معتبر';
            break;
        }
      }
      return errors;
    }),
  code: joi
    .number()
    .required()
    .error((errors) => {
      for (const err of errors) {
        switch (err.code) {
          default:
            err.message = 'کد نامعتبر است';
            break;
        }
      }
      return errors;
    }),
});

const schemaCompiler = (schema) => (data) => {
  const { error, value } = schema.validate(data, joiOptions);

  if (error) {
    return { error };
  }
  return { value };
};

const schema = {
  body: joiBodySchema,
};

module.exports = {
  schema,
  schemaCompiler: schemaCompiler,
};
