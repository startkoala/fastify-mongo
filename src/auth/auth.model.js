const mongoose = require('mongoose');

const AuthSchema = new mongoose.Schema({
  phoneNumber: String,
  verifyCode: Number,
  verified: {
    type: Boolean,
    default: false,
  },
});

const Model = mongoose.model('auth', AuthSchema);

module.exports = Model;
