const jwt = require('jsonwebtoken');
const config = require('config');
const Auth = require('./auth.model');
const User = require('../users/user.model');

exports.findAuth = (phoneNumber) => {
  return Auth.findOne({ phoneNumber });
};

exports.generateCode = () => {
  return Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
};

exports.submitVerifyCode = (phoneNumber, verifyCode) => {
  return Auth.findOneAndUpdate(
    { phoneNumber },
    { $set: { verifyCode } },
    { upsert: true }
  );
};

exports.submitUserVerifyCode = (phoneNumber, verifyCode) => {
  return User.findOneAndUpdate({ phoneNumber }, { $set: { verifyCode } });
};

exports.registerUser = async (user) => {
  await Auth.findOneAndRemove({ phoneNumber: user.phoneNumber });
  return User.create(user);
};

exports.generateToken = async (user) => {
  return jwt.sign({ id: user.id }, config.get('jwtSecret'), {
    expiresIn: '30d',
  });
};
