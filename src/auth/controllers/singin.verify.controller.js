// entities
const entities = require('../auth.entities');
const userEntities = require('../../users/user.entities');

// utils
// const sms = require('../../../utils/sms')

const handle = async (req) => {
  try {
    const { phoneNumber, code } = req.body;

    // search for existing phone number
    const user = await userEntities.findByPhoneNumber(phoneNumber);
    if (!user) {
      const error = new Error('لطفا دوباره درخواست ارسال کد فعال سازی بدهید');
      error.statusCode = 400;
      return error;
    }

    // check verify code
    if (user.verifyCode !== code) {
      const error = new Error('کد وارد شده صحیح نیست');
      error.statusCode = 400;
      return error;
    }

    // generate token
    const token = await entities.generateToken(user);

    return {
      statusCode: 200,
      message: 'ورود موفقیت آمیز بود',
      user: {
        name: user.name,
        phoneNumber: user.phoneNumber,
      },
      token,
    };
  } catch (err) {
    err.location = 'singup.verify.controller.js';
    return err;
  }
};

module.exports = handle;
