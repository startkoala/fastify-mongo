// entities
const entities = require('../auth.entities');
const userEntities = require('../../users/user.entities');

// utils
// const sms = require('../../../utils/sms')

const handle = async (req) => {
  try {
    const { phoneNumber } = req.body;

    // search for existing phone number
    const result = await userEntities.findByPhoneNumber(phoneNumber);
    if (result) {
      const error = new Error('امکان ثبت نام با این شماره وجود ندارد');
      error.statusCode = 400;
      return error;
    }

    // submit verify code to db and send verify code to phoneNumber
    const verifyCode = entities.generateCode();
    await entities.submitVerifyCode(phoneNumber, verifyCode);

    // TODO uncomment me
    // sms.SendMessageForVerify(phoneNumber, verifyCode, 172)

    return {
      statusCode: 200,
      message: 'پیامک حاوی کد فعال سازی برای شما ارسال شد',
    };
  } catch (err) {
    err.location = 'singup.otp.controller.js';
    return err;
  }
};

module.exports = handle;
