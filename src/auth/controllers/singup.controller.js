// entities
const entities = require('../auth.entities');

const handle = async (req) => {
  try {
    // check verify status
    const result = await entities.findAuth(req.body.phoneNumber);
    if (!result.verified) {
      const error = new Error('شماره شما تایید نشده');
      error.statusCode = 400;
      return error;
    }

    // remove user info from register collection and create user
    const user = await entities.registerUser(req.body);

    // generate token
    const token = await entities.generateToken(user);

    return {
      statusCode: 200,
      message: 'ثبت نام با موفقیت انجام شد',
      user: {
        name: user.name,
        phoneNumber: user.phoneNumber,
      },
      token,
    };
  } catch (err) {
    err.location = 'singup.controller.js';
    return err;
  }
};

module.exports = handle;
