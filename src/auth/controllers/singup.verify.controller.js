// entities
const entities = require('../auth.entities');

// utils
// const sms = require('../../../utils/sms')

const handle = async (req) => {
  try {
    const { phoneNumber, code } = req.body;

    // search for existing phone number
    const result = await entities.findAuth(phoneNumber);
    if (!result) {
      const error = new Error('لطفا دوباره درخواست ارسال کد فعال سازی بدهید');
      error.statusCode = 400;
      return error;
    }

    // check verify code
    if (result.verifyCode !== code) {
      const error = new Error('کد وارد شده صحیح نیست');
      error.statusCode = 400;
      return error;
    }

    // verifying user
    result.verified = true;
    await result.save();

    return {
      statusCode: 200,
      message: 'شماره شما تایید شد ، لطفا اطلاعات خود را وارد کنید',
    };
  } catch (err) {
    err.location = 'singup.verify.controller.js';
    return err;
  }
};

module.exports = handle;
