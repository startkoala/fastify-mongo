// error handler
const ErrorHandler = require('../../utils/ErrorHandler');

// otp
const singupDto = require('./dto/singup.dto');
const singupDtoOtp = require('./dto/singupOtp.dto');
const singupVerifyOtp = require('./dto/singupVerify.dto');

// handler
const singinOtp = require('./controllers/singin.otp.controller');
const singinVerify = require('./controllers/singin.verify.controller');
const singup = require('./controllers/singup.controller');
const singupOtp = require('./controllers/singup.otp.controller');
const singupVerify = require('./controllers/singup.verify.controller');

const router = (fastify, opt, done) => {
  // routes

  // singup
  fastify.post('/singup/otp', { ...singupDtoOtp }, singupOtp);
  fastify.post('/singup/verify', { ...singupVerifyOtp }, singupVerify);
  fastify.post('/singup', { ...singupDto }, singup);

  // singin
  fastify.post('/singin/otp', { ...singupDtoOtp }, singinOtp);
  fastify.post('/singin/verify', { ...singupVerifyOtp }, singinVerify);

  // Error handeling
  fastify.setErrorHandler(ErrorHandler);

  done();
};

module.exports = router;
