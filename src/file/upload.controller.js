const mkdirp = require('mkdirp');

const handle = async (req, res) => {
  try {
    const { files } = req.raw;
    const fileArr = [];
    mkdirp.sync('public/upload/images');
    // eslint-disable-next-line guard-for-in
    for (const key in files) {
      // generate file name
      const fileName = `${Date.now()}-${Math.floor(Math.random() * 100000)}-${
        files[key].name
      }`;
      const fileUrl = `upload/images/${fileName}`;
      const filePath = `public/${fileUrl}`;

      // check file format
      if (
        files[key].mimetype === 'image/jpeg' ||
        files[key].mimetype === 'image/jpg' ||
        files[key].mimetype === 'image/png'
      ) {
        files[key].mv(filePath);
        fileArr.push({
          name: files[key].name,
          url: fileUrl,
        });
      }
    }
    res.send(fileArr);
  } catch (err) {
    err.location = 'users => upload.controller.js';
    return err;
  }
};

module.exports = handle;
