const mongoose = require('mongoose');

const File = new mongoose.Schema(
  {
    type: {
      type: String,
    },
    url: {
      type: String,
    },
    uploader: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
  },
  { timestamps: true, toJSON: { virtuals: true } }
);

module.exports = mongoose.model('file', File);
