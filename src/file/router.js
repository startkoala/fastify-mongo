// error handler
const ErrorHandler = require('../../utils/ErrorHandler');

// middlerware
const checkToken = require('../../middleware/CheckToken');

// handler
const uploadController = require('./upload.controller');

const router = (fastify, opt, done) => {
  // routes
  fastify.post(
    '/',

    uploadController
  );
  // Error handeling
  fastify.setErrorHandler(ErrorHandler);

  done();
};

module.exports = router;
