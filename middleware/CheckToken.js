const verify = require('../utils/verifyToken');
const userEntities = require('../src/users/user.entities');

const handle = async (req, res, done) => {
  try {
    // check Authorization exist
    const Authorization = req.headers.authorization;
    if (!Authorization) throw new Error();

    // get token
    const token = Authorization.split(' ')[1];

    // verify token
    const userId = await verify(token);
    if (!userId) throw new Error();

    // search user
    const user = await userEntities.findById(userId);
    if (!user) throw new Error();

    // set user for handler
    req.user = user;

    return done();
  } catch (err) {
    const error = new Error('Unauthorized');
    error.statusCode = 401;
    return res.send(error);
  }
};

module.exports = handle;
