const mongoose = require('mongoose');
const config = require('config');
const chalk = require('chalk');
const helmet = require('fastify-helmet');
const path = require('path');
const fileUpload = require('fastify-file-upload');
const fastify = require('fastify')({
  logger: false,
});

// Developer Info
fastify.get('/', (req, res) => {
  res.send({
    developer: {
      name: 'Mohammad Sadeghian',
      phoneNumber: '09035436306',
      socialMedia: '@mosa_5445',
      email: 'mosa5445@gmail.com',
    },
    projectName: 'fastify-test',
    startDate: '1399/03/14',
  });
});

// setup db
const dbconfig = config.get('database');
mongoose.Promise = global.Promise;
mongoose.connect(
  `mongodb://${dbconfig.host}:${dbconfig.port}/${dbconfig.dbName}`,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  },
  (err) => {
    if (err) {
      console.log(chalk.bgRed.white('can not connect to db'));
      console.log(err);
    } else {
      console.log(
        chalk.blackBright(
          `DB connected to ${dbconfig.host}:${dbconfig.port}/${dbconfig.dbName}`
        )
      );
    }
  }
);

// middlewares
fastify.register(helmet);
fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/public/', // optional: default '/'
});

fastify.register(fileUpload, {
  limits: { fileSize: 5 * 1024 * 1024 },
  safeFileNames: true,
  preserveExtension: true,
  useTempFiles: true,
});

fastify.get('/upload/:category/:fileName', (req, reply) => {
  reply.sendFile(`upload/${req.params.category}/${req.params.fileName}`)
});

// api v1 routes
fastify.register(require('./router'), { prefix: '/v1' });

// listener
fastify.listen(4000, (err, address) => {
  if (err) console.log(err);
  else console.log(chalk.blackBright(`server started on ${address}`));
});
