// routes
const auth = require('./src/auth/router');
const upload = require('./src/file/router');

const router = (fastify, opt, done) => {
  fastify.register(auth, { prefix: '/auth' });
  fastify.register(upload, { prefix: '/upload' });

  done();
};

module.exports = router;
