const chalk = require('chalk');

const ErrorHandler = (statusCode, message, error, location) => {
  // Handle 5xx Errors
  if (statusCode >= 500) {
    console.log(chalk.bgWhite.red(`\n${location}`));
    console.log(chalk.white.bgRed(message));
    return {
      statusCode: statusCode,
      message: 'خطایی رخ داده ، لطفا با پشتیبانی تماس بگیرید',
    };
  }

  // Handle 4xx Errors
  return {
    statusCode: statusCode,
    message,
    errors: error && error.length ? error : undefined,
  };
};

const handler = (error, req, res) => {
  if (error.details) {
    const errors = [];

    error.details.map((err) =>
      errors.push({
        key: err.context.key,
        message: err.message,
        value: err.context.value,
      })
    );
    res.send(ErrorHandler(400, 'Validation Error', errors));
  } else
    res.send(
      ErrorHandler(
        error.statusCode || 500,
        error.message || 'Internal Server Error',
        error.errors,
        error.location
      )
    );
};

module.exports = handler;
