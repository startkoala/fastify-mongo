/* eslint-disable */
const unirest = require('unirest');

module.exports = {
  getApiTokenUrl() {
    return 'http://RestfulSms.com/api/Token';
  },
  getAPIUltraFastSendUrl() {
    return 'http://RestfulSms.com/api/UltraFastSend';
  },

  /* Send Message */
  UltraFastSend(data, callback) {
    function execute(postData, url, token, callback) {
      unirest
        .post(url)
        .headers({
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-sms-ir-secure-token': token,
        })
        .send(postData)
        .end(function (response) {
          return callback(response.body);
        });
    }

    const APIUltraFastSendUrl = this.getAPIUltraFastSendUrl();
    this.GetToken(function (token) {
      execute(data, APIUltraFastSendUrl, token['TokenKey'], function (
        exResult
      ) {
        return callback(exResult);
      });
    });
  },
  /* Get Token */
  GetToken(callback) {
    const headers = {
      'Content-Type': 'application/json',
    };

    const options = {
      uri: this.getApiTokenUrl(),
      method: 'POST',
      header: headers,
      json: {
        UserApiKey: `b78c72d0c19fea72c8906e91`,
        SecretKey: `sdfjDSS*&%Dsdf453#%FDF`,
      },
    };
    unirest.request(options, function (error, response, body) {
      return callback(body);
    });
  },

  SendMessageForVerify(phoneNumber, randomNumber, TempId) {
    const msgData = {
      ParameterArray: [
        {
          Parameter: 'VerificationCode',
          ParameterValue: randomNumber,
        },
      ],
      Mobile: phoneNumber,
      TemplateId: TempId,
    };
    this.UltraFastSend(msgData, function (messageData) {});
  },

  async SendMessageForAdmin(orderId) {
    // const dbHelper = require('../modules/dbHelper');
    // let ManagerPhone = await dbHelper.find('SELECT ManagerPhone FROM Config');
    const msgData = {
      ParameterArray: [
        {
          Parameter: 'number',
          ParameterValue: orderId,
        },
      ],
      Mobile: '09369827262',
      TemplateId: 315,
    };
    this.UltraFastSend(msgData, function (messageData) {});
  },
};
