const jwt = require('jsonwebtoken');
const confing = require('config');

const handle = (token, ignoreExpire = false) => {
  try {
    return jwt.verify(token, confing.get('jwtSecret'), {
      ignoreExpiration: ignoreExpire,
    }).id;
  } catch (err) {
    return err;
  }
};

module.exports = handle;
